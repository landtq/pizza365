// Import thư viện mongoose
const mongoose = require('mongoose');

// Khai báo class Schema
const Schema = mongoose.Schema;

// Khai báo drink schema
const drinkModel = new Schema({
     _id: {
          type: mongoose.Types.ObjectId
     },
     maNuocUong: {
          type: String,
          unique: true,
          required: true
     },
     tenNuocUong: {
          type: String,
          required: true
     },
     donGia: {
          type: Number,
          required: true
     }
}, {
     timestamps: true
})

module.exports = mongoose.model('drinks', drinkModel);