// Import thư viện mongoose
const mongoose = require('mongoose');

// Khai báo class Schema
const Schema = mongoose.Schema;

// Khai báo order schema
const orderModel = new Schema({
     _id: {
          type: mongoose.Types.ObjectId
     },
     orderCode: {
          type: String,
          required: true,
          unique: true
     },
     pizzaSize: {
          type: String,
          required: true
     },
     pizzaType: {
          type: String,
          required: true
     },
     voucher: {
          type: String,
          ref: 'Voucher'
     },
     drink: {
          type: String,
          ref: 'Drink'
     },
     status: {
          type: String,
          required: true
     }
}, {
     timestamps: true
})

module.exports = mongoose.model('order', orderModel);