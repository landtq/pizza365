// Import thư viện mongoose
const mongoose = require('mongoose');

// Import drink model
const drinkModel = require('../models/drinkModel'); 

// Get all drinks
const getAllDrinks = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     // B2: Validate dữ liệu
     // B3: Gọi model thực hiện các thao tác nghiệp vụ
     drinkModel.find((error, data) => {
          if (error) {
               return res.status(500).json({
                    message: error.message
               })
          }
          return res.status(200).json({
               message: 'Get all drinks',
               drinks: data
          })
     })
}

// Get drink by ID
const getDrinkByID = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     let drinkID = req.params.drinkID;
     // B2: Validate dữ liệu
     if (!mongoose.Types.ObjectId.isValid(drinkID)) {
          return res.status(400).json({
               message: 'Drink ID is invalid!'
          })
     }
     // B3: Gọi model thực hiện các thao tác nghiệp vụ
     drinkModel.findById(drinkID, (error, data) => {
          if (error) {
               return res.status(500).json({
                    message: error.message
               })
          }
          return res.status(200).json({
               message: 'Get a drink by ID',
               drink: data
          })
     })
}

// Create a drink
const createDrink = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     let body = req.body;
     // B2: Validate dữ liệu
     if (!body.maNuocUong) {
          return res.status(400).json({
               message: 'Drink Code is required!'
          })
     }
     if (!body.tenNuocUong) {
          return res.status(400).json({
               message: 'Drink Name is required!'
          })
     }
     if (!Number.isInteger(body.donGia) || body.donGia < 0) {
          return res.status(400).json({
               message: 'Drink Price is invalid!'
          })
     }
     // B3: Gọi model thực hiện các thao tác nghiệp vụ
     let newDrink = {
          _id: mongoose.Types.ObjectId(),
          maNuocUong: body.maNuocUong,
          tenNuocUong: body.tenNuocUong,
          donGia: body.donGia
     }
     drinkModel.create(newDrink, (error, data) => {
          if (error) {
               return res.status(500).json({
                    message: error.message
               })
          }
          return res.status(201).json({
               message: 'Create successfully',
               newDrink: data
          })
     })
}

// Update a drink
const updateDrink = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     let drinkID = req.params.drinkID;
     let body = req.body;
     // B2: Validate dữ liệu
     if (!mongoose.Types.ObjectId.isValid(drinkID)) {
          return res.status(400).json({
               message: 'Drink ID is invalid!'
          })
     }
     if (body.maNuocUong !== undefined && body.maNuocUong == "") {
          return res.status(400).json({
               message: 'Drink Code is required!'
          })
     }
     if (body.tenNuocUong !== undefined && body.tenNuocUong == "") {
          return res.status(400).json({
               message: 'Drink Name is required!'
          })
     }
     if (body.donGia !== undefined && !Number.isInteger(body.donGia) || body.donGia < 0) {
          return res.status(400).json({
               message: 'Drink Price is invalid!'
          })
     }
     // B3: Gọi model thực hiện các thao tác nghiệp vụ
     let drinkUpdate = {
          maNuocUong: body.maNuocUong,
          tenNuocUong: body.tenNuocUong,
          donGia: body.donGia
     }
     drinkModel.findByIdAndUpdate(drinkID, drinkUpdate, (error, data) => {
          if (error) {
               return res.status(500).json({
                    message: error.message
               })
          }
          return res.status(200).json({
               message: 'Update successfully',
               updateDrink: data
          })
     })
}

// Delete a drink
const deleteDrink = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     let drinkID = req.params.drinkID;
     // B2: Validate dữ liệu
     if (!mongoose.Types.ObjectId.isValid(drinkID)) {
          return res.status(400).json({
               message: 'Drink ID is invalid!'
          })
     }
     // B3: Gọi model thực hiện các thao tác nghiệp vụ
     drinkModel.findByIdAndDelete(drinkID, (error, data) => {
          if (error) {
               return res.status(500).json({
                    message: error.message
               })
          }
          return res.status(204).json({
               message: 'Delete successfully'
          })
     })
}

module.exports = { getAllDrinks, getDrinkByID, createDrink, updateDrink, deleteDrink }