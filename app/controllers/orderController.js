// Import thư viện mongoose
const mongoose = require('mongoose');

// Import models
const orderModel = require('../models/orderModel');
const userModel = require('../models/userModel');

// Get all orders of an user
const getAllOrdersOfAnUser = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     let userID = req.params.userID;
     // B2: Validate dữ liệu
     if (!mongoose.Types.ObjectId.isValid(userID)) {
          return res.status(400).json({
               message: 'User ID is invalid!'
          })
     }
     // B3: Gọi model thực hiện các thao tác nghiệp vụ
     userModel.findById(userID)
          .populate('orders')
          .exec((error, data) => {
               if (error) {
                    return res.status(500).json({
                         message: error.message
                    })
               }
               return res.status(200).json({
                    message: 'Get all orders of an user successfully',
                    orders: data.orders
               })
          })
}

// Get an order
const getOrderByOrderID = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     let orderID = req.params.orderID;
     // B2: Validate dữ liệu
     if (!mongoose.Types.ObjectId.isValid(orderID)) {
          return res.status(400).json({
               message: 'Order ID is invalid!'
          })
     }
     // B3: Gọi model thực hiện các thao tác nghiệp vụ
     orderModel.findById(orderID, (error, data) => {
          if (error) {
               return res.status(500).json({
                    message: error.message
               })
          }
          return res.status(200).json({
               message: 'Get an order by order ID successfully',
               order: data
          })
     })
}

// Create a new order
const createOrder = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     let userID = req.params.userID;
     let body = req.body;
     // B2: Validate dữ liệu
     if (!mongoose.Types.ObjectId.isValid(userID)) {
          return res.status(400).json({
               message: 'User ID is invalid!'
          })
     }
     if (!body.orderCode) {
          return res.status(400).json({
               message: 'Order Code is required!'
          })
     }
     if (!body.pizzaSize) {
          return res.status(400).json({
               message: 'Pizza Size is required!'
          })
     }
     if (!body.pizzaType) {
          return res.status(400).json({
               message: 'Pizza Type is required!'
          })
     }
     if (!body.status) {
          return res.status(400).json({
               message: 'Status is required!'
          })
     }
     // B3: Gọi model thực hiện các thao tác nghiệp vụ
     let newOrder = {
          _id: mongoose.Types.ObjectId(),
          orderCode: body.orderCode,
          pizzaSize: body.pizzaSize,
          pizzaType: body.pizzaType,
          voucher: body.voucher,
          drink: body.drink,
          status: body.status
     }
     orderModel.create(newOrder, (error, data) => {
          if (error) {
               return res.status(500).json({
                    message: error.message
               })
          } else {
               userModel.findByIdAndUpdate(userID,
                    {
                         $push: { orders: data._id }
                    },
                    (err, updatedOrder) => {
                         if (err) {
                              return res.status(500).json({
                                   message: error.message
                              })
                         } else {
                              return res.status(201).json({
                                   message: 'Create a new order successfully',
                                   order: data
                              })
                         }
                    })
          }
     })
}

// Update an order
const updateOrder = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     let orderID = req.params.orderID;
     let body = req.body;
     // B2: Validate dữ liệu
     if (!mongoose.Types.ObjectId.isValid(orderID)) {
          return res.status(400).json({
               message: 'Order ID is invalid!'
          })
     }
     if (body.orderCode !== undefined && body.orderCode == '') {
          return res.status(400).json({
               message: 'Order Code is required!'
          })
     }
     if (body.pizzaSize !== undefined && body.pizzaSize == '') {
          return res.status(400).json({
               message: 'Pizza Size is required!'
          })
     }
     if (body.pizzaType !== undefined && body.pizzaType == '') {
          return res.status(400).json({
               message: 'Pizza Type is required!'
          })
     }
     if (body.status !== undefined && body.status == '') {
          return res.status(400).json({
               message: 'Status is required!'
          })
     }
     // B3: Gọi model thực hiện các thao tác nghiệp vụ
     let orderUpdated = {
          orderCode: body.orderCode,
          pizzaSize: body.pizzaSize,
          pizzaType: body.pizzaType,
          voucher: body.voucher,
          drink: body.drink,
          status: body.status
     }
     orderModel.findByIdAndUpdate(orderID, orderUpdated, (error, data) => {
          if (error) {
               return res.status(500).json({
                    message: error.message
               })
          }
          return res.status(200).json({
               message: 'Update an order successfully',
               order: data
          })
     })
}

// Delete an order
const deleteOrder = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     let userID = req.params.userID;
     let orderID = req.params.orderID;
     // B2: Validate dữ liệu
     if (!mongoose.Types.ObjectId.isValid(userID)) {
          return res.status(400).json({
               message: 'User ID is invalid!'
          })
     }
     if (!mongoose.Types.ObjectId.isValid(orderID)) {
          return res.status(400).json({
               message: 'Order ID is invalid!'
          })
     }
     // B3: Gọi model thực hiện các thao tác nghiệp vụ
     orderModel.findByIdAndDelete(orderID, (error, data) => {
          if (error) {
               return res.status(500).json({
                    message: error.message
               })
          } else {
               userModel.findByIdAndUpdate(userID,
                    {
                         $pull: { orders: orderID }
                    },
                    (err, updatedUser) => {
                         if (err) {
                              return res.status(500).json({
                                   message: err.message
                              })
                         }
                         return res.status(204).json({
                              message: 'Delete an order successfully'
                         })
                    })
          }
     })
}

module.exports = { getAllOrdersOfAnUser, getOrderByOrderID, createOrder, updateOrder, deleteOrder };