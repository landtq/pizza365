// Import thư viện mongoose
const mongoose = require('mongoose');

// Import model
const userModel = require('../models/userModel');

// Get all users
const getAllUsers = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     // B2: Validate dữ liệu
     // B3: Gọi model thực hiện các thao tác nghiệp vụ
     userModel.find((error, data) => {
          if (error) {
               return res.status(500).json({
                    message: error.message
               })
          }
          return res.status(200).json({
               message: 'Get all users successfully',
               users: data
          })
     })
}

// Get an user by ID
const getUserByID = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     let userID = req.params.userID;
     // B2: Validate dữ liệu
     if (!mongoose.Types.ObjectId.isValid(userID)) {
          return res.status(400).json({
               message: 'User ID is invalid!'
          })
     }
     // B3: Gọi model thực hiện các thao tác nghiệp vụ
     userModel.findById(userID, (error, data) => {
          if (error) {
               return res.status(500).json({
                    message: error.message
               })
          }
          return res.status(200).json({
               message: 'Get an user by ID successfully',
               user: data
          })
     })
}

// Create a new user
const createUser = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     let body = req.body;
     // B2: Validate dữ liệu
     if (!body.fullName) {
          return res.status(400).json({
               message: 'Full Name is required!'
          })
     }
     const emailRegexp = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
     if (!emailRegexp.test(body.email)) {
          return res.status(400).json({
               message: 'Email is invalid!'
          })
     }
     if (!body.address) {
          return res.status(400).json({
               message: 'Address is required!'
          })
     }
     if (!body.phone) {
          return res.status(400).json({
               message: 'Phone is required!'
          })
     }
     // B3: Gọi model thực hiện các thao tác nghiệp vụ
     let newUser = {
          _id: mongoose.Types.ObjectId(),
          fullName: body.fullName,
          email: body.email,
          address: body.address,
          phone: body.phone
     }
     userModel.create(newUser, (error, data) => {
          if (error) {
               return res.status(500).json({
                    message: error.message
               })
          }
          return res.status(201).json({
               message: 'Create a new user successfully',
               newUser: data
          })
     })
}

// Update an user
const updateUser = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     let userID = req.params.userID;
     let body = req.body;
     // B2: Validate dữ liệu
     if (!mongoose.Types.ObjectId.isValid(userID)) {
          return res.status(400).json({
               message: 'User ID is invalid!'
          })
     }
     if (body.fullName !== undefined && body.fullName == "") {
          return res.status(400).json({
               message: 'Full Name is required!'
          })
     }
     const emailRegexp = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
     if (!emailRegexp.test(body.email)) {
          return res.status(400).json({
               message: 'Email is invalid!'
          })
     }
     if (body.address !== undefined && body.address == "") {
          return res.status(400).json({
               message: 'Address is required!'
          })
     }
     if (body.phone !== undefined && body.phone == "") {
          return res.status(400).json({
               message: 'Phone is required!'
          })
     }
     // B3: Gọi model thực hiện các thao tác nghiệp vụ
     let userUpdated = {
          fullName: body.fullName,
          email: body.email,
          address: body.address,
          phone: body.phone
     }
     userModel.findByIdAndUpdate(userID, userUpdated, (error, data) => {
          if (error) {
               return res.status(500).json({
                    message: error.message
               })
          }
          return res.status(200).json({
               message: 'Update an user by ID successfully',
               user: data
          })
     })
}

// Delete an user
const deleteUser = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     let userID = req.params.userID;
     // B2: Validate dữ liệu
     if (!mongoose.Types.ObjectId.isValid(userID)) {
          return res.status(400).json({
               message: 'User ID is invalid!'
          })
     }
     // B3: Gọi model thực hiện các thao tác nghiệp vụ
     userModel.findByIdAndDelete(userID, (error, data) => {
          if (error) {
               return res.status(500).json({
                    message: error.message
               })
          }
          return res.status(204).json({
               message: 'Delete an user by ID successfully',
          })
     })
}

// Skip - Limit - Sort
// Get all user limit
const getAllUsersLimit = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     let limit = req.query.limit;
     limit = parseInt(limit);
     // B2: Validate dữ liệu
     if (!Number.isInteger(limit)) {
          return res.status(400).json({
               message: 'Limit must be a number!'
          })
     }
     // B3: Gọi model thực hiện các thao tác nghiệp vụ
     userModel.find()
          .limit(limit)
          .exec((error, data) => {
               if (error) {
                    return res.status(500).json({
                         message: error.message
                    })
               } else {
                    if (limit) {
                         return res.status(200).json({
                              message: `Get limit ${limit} users successfully`,
                              users: data
                         })
                    } else {
                         return res.status(200).json({
                              message: 'Get all users',
                              users: data
                         })
                    }
               }
          })
}

// Get all user skip
const getAllUsersSkip = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     let skip = req.query.skip;
     skip = parseInt(skip);
     // B2: Validate dữ liệu
     if (!Number.isInteger(skip)) {
          return res.status(400).json({
               message: 'Skip users must be a number!'
          })
     }
     // B3: Gọi model thực hiện các thao tác nghiệp vụ
     userModel.find()
          .skip(skip)
          .exec((error, data) => {
               if (error) {
                    return res.status(500).json({
                         message: error.message
                    })
               } else {
                    if (skip) {
                         return res.status(200).json({
                              message: `Skip ${skip} users successfully`,
                              users: data
                         })
                    } else {
                         return res.status(200).json({
                              message: 'Get all users',
                              users: data
                         })
                    }
               }
          })
}

// Get all users sorted
const getAllUsersSortedByFullName = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     // B2: Validate dữ liệu
     // B3: Gọi model thực hiện các thao tác nghiệp vụ
     userModel.find()
          .sort({ fullName: 'asc' })
          .exec((error, data) => {
               if (error) {
                    return res.status(500).json({
                         message: error.message
                    })
               }
               return res.status(200).json({
                    message: 'Get all users sorted by full name successfully',
                    users: data
               })
          })
}

// Get all users skip limit
const getAllUsersSkipLimit = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     let limit = req.query.limit;
     let skip = req.query.skip;
     limit = parseInt(limit);
     skip = parseInt(skip);
     // B2: Validate dữ liệu
     if (!Number.isInteger(limit)) {
          return res.status(400).json({
               message: 'Limit must be a number!'
          })
     }
     if (!Number.isInteger(skip)) {
          return res.status(400).json({
               message: 'Skip users must be a number!'
          })
     }
     // B3: Gọi model thực hiện các thao tác nghiệp vụ
     userModel.find()
          .skip(skip)
          .limit(limit)
          .exec((error, data) => {
               if (error) {
                    return res.status(500).json({
                         message: error.message
                    })
               } else {
                    if (limit && skip) {
                         return res.status(200).json({
                              message: `Get skip ${skip} users and limit ${limit} users successfully`,
                              users: data
                         })
                    } else {
                         return res.status(200).json({
                              message: 'Get all users successfully',
                              users: data
                         })
                    }
               }
          })
}

// Get all users sorted skip limit
const getAllUsersSortedSkipLimit = (req, res) => {
     // B1: Thu thập dữ liệu từ req
     let limit = req.query.limit;
     let skip = req.query.skip;
     limit = parseInt(limit);
     skip = parseInt(skip);
     // B2: Validate dữ liệu
     if (!Number.isInteger(limit)) {
          return res.status(400).json({
               message: 'Limit must be a number!'
          })
     }
     if (!Number.isInteger(skip)) {
          return res.status(400).json({
               message: 'Skip users must be a number!'
          })
     }
     // B3: Gọi model thực hiện các thao tác nghiệp vụ
     userModel.find()
          .sort({ fullName: 'asc' })
          .skip(skip)
          .limit(limit)
          .exec((error, data) => {
               if (error) {
                    return res.status(500).json({
                         message: error.message
                    })
               }
               return res.status(200).json({
                    message: 'Get users successfully',
                    users: data
               })

          })
}

module.exports = {
     getAllUsers, getUserByID, createUser, updateUser, deleteUser, getAllUsersLimit,
     getAllUsersSkip, getAllUsersSortedByFullName, getAllUsersSkipLimit, getAllUsersSortedSkipLimit
};