// Import thư viện express
const express = require('express');
const { voucherMiddleware } = require('../middlewares/voucherMiddleware');

// Import controllers
const {
     getAllVouchers,
     getVoucherByID,
     createVoucher,
     updateVoucher,
     deleteVoucher
} = require('../controllers/voucherController');

// Tạo router
const voucherRouter = express.Router();

// Sử dụng middleware
voucherRouter.use(voucherMiddleware);

// Get all vouchers
voucherRouter.get('/vouchers', getAllVouchers);

// Get voucher by ID
voucherRouter.get('/vouchers/:voucherID', getVoucherByID)

// Create a new voucher
voucherRouter.post('/vouchers', createVoucher);

// Update a voucher
voucherRouter.put('/vouchers/:voucherID', updateVoucher)

// Delete a voucher
voucherRouter.delete('/vouchers/:voucherID', deleteVoucher);

module.exports = { voucherRouter };