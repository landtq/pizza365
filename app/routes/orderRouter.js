// Import thư viện express
const express = require('express');

// Import middleware
const { orderMiddleware } = require('../middlewares/orderMiddleware');

// Import controller
const { getAllOrdersOfAnUser,
     getOrderByOrderID,
     createOrder,
     updateOrder,
     deleteOrder } = require('../controllers/orderController');

// Tạo router
const orderRouter = express.Router();

// Sử dụng middleware
orderRouter.use(orderMiddleware);

// Get all orders of an user
orderRouter.get('/users/:userID/orders', getAllOrdersOfAnUser);

// Get an order
orderRouter.get('/orders/:orderID', getOrderByOrderID);

// Create a new order
orderRouter.post('/users/:userID/orders', createOrder);

// Update an order
orderRouter.put('/orders/:orderID', updateOrder);

// Delete an order
orderRouter.delete('/users/:userID/orders/:orderID', deleteOrder);

module.exports = { orderRouter };