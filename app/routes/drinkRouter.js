// Import thư viện express
const express = require('express');
const { drinkMiddleware } = require('../middlewares/drinkMiddleware');

// Import controllers
const {
     getAllDrinks,
     getDrinkByID,
     createDrink,
     updateDrink,
     deleteDrink
} = require('../controllers/drinkController');

// Tạo router
const drinkRouter = express.Router();

// Sử dụng middleware
drinkRouter.use(drinkMiddleware);

// Get all drinks
drinkRouter.get('/drinks', getAllDrinks);

// Get drink by ID
drinkRouter.get('/drinks/:drinkID', getDrinkByID);

// Create a drink
drinkRouter.post('/drinks', createDrink);

// Update a drink
drinkRouter.put('/drinks/:drinkID', updateDrink);

// Delete a drink
drinkRouter.delete('/drinks/:drinkID', deleteDrink);

module.exports = { drinkRouter };