// Import thư viện express
const express = require('express');

// Tạo router
const order2Router = express.Router();

// Import controllers
const { createOrderWithOrderCode,
     getDrinkList } = require('../controllers/order2Controller');

// Create order
order2Router.post('/devcamp-pizza365/orders', createOrderWithOrderCode);

// Get drink list
order2Router.get('/devcamp-pizza365/drinks', getDrinkList);

module.exports = { order2Router };