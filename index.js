// Import thư viện express
const express = require('express');

// Import thư viện path
const path = require('path');

// Import thư viện mongoose
const mongoose = require('mongoose');

// Import router modules
const { drinkRouter } = require('./app/routes/drinkRouter');
const { voucherRouter } = require('./app/routes/voucherRouter');
const { userRouter } = require('./app/routes/userRouter');
const { orderRouter } = require('./app/routes/orderRouter');
const { order2Router } = require('./app/routes/order2Router');

mongoose.connect('mongodb://localhost:27017/CRUD_Test_Pizza365', (error) => {
     if (error) throw error;
     console.log('Connected Successfully');
})

// Tạo app
const app = express();

// Tạo cổng chạy prj
const port = 8000;

// Khai báo API dạng GET "/" để chạy homepage
app.get("/", (req, res) => {
     console.log(__dirname);
 
     res.sendFile(path.join(__dirname + "/views/Pizza 365.html"));
})

// Thêm middleware static vào để hiển thị ảnh
app.use(express.static(__dirname + "/views"));

// Khai báo API để chạy Order List
app.get("/orderList", (req, res) => {
     console.log(__dirname);

     res.sendFile(path.join(__dirname + "/views/OrderList.html"));
})

// Sử dụng body json
app.use(express.json());

// Sử dụng đc unicode
app.use(express.urlencoded({
     extended: true
}))

// Sử dụng router
app.use(drinkRouter);
app.use(voucherRouter);
app.use(userRouter);
app.use(orderRouter);
app.use(order2Router);

app.listen(port, () => {
     console.log(`App listening to port ${port}`);
})